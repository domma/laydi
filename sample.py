from laydi import *


sample = Horizontal(
    Vertical(
        Text("Hello World!"),
        Border(Text("Short"), 10),
        Text("Some rather long text ...")
    ),
    Vertical(
        Text("He"),
        Horizontal(Text("aaaaa"), Border(Text("werqerqe"),0, align="right")),
        Border(Text("Short"), 5),
        Text("Some rather long asdfadfasdfasdfasdf text ...")
    ),
    Vertical(
        Text("World!", size=15),
        Border(Text("xxx", size=20), 10, align='right'),
        Text("Some rather long long long text ...")
    )
)

render_png(sample, "out.png")
