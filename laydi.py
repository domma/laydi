import qahirah as q


class LayoutElement:
    width = 0
    height = 0

    def measure(self, ctx):
        pass

    def draw(self, ctx, x, y, width, height):
        pass


class Container(LayoutElement):
    children = None

    def measure(self, ctx):
        for c in self.children:
            c.measure(ctx)


class Border(LayoutElement):
    def __init__(self, child, size, align='center'):
        self.child = child
        self.size = size
        self.align = align

    def measure(self, ctx):
        self.child.measure(ctx)

    @property
    def width(self):
        return 2*self.size + self.child.width

    @property
    def height(self):
        return 2*self.size + self.child.height

    def draw(self, ctx, x, y, width, height):
        if self.align=='left':
            left = 0
        else:
            w = width - 2*self.size - self.child.width
            if self.align=='center':
                left = w / 2
            elif self.align=='right':
                left = w
        self.child.draw(ctx, x+self.size+left, y+self.size, width - 2*self.size, height - 2*self.size)


class Text(LayoutElement):
    def __init__(self, text, font="Georgia", size=12):
        self.text = text
        self.font = font
        self.size = size


    def measure(self, ctx):
        ctx.select_font_face(self.font, q.CAIRO.FONT_SLANT_NORMAL, q.CAIRO.FONT_WEIGHT_BOLD)
        ctx.set_font_size(self.size)
        self.width = ctx.text_extents(self.text).bounds.width
        ext = ctx.font_extents
        self.height = ext.ascent + ext.descent
        self.ascent = ext.ascent

    
    def draw(self, ctx, x, y, width, height):
        ctx.select_font_face(self.font, q.CAIRO.FONT_SLANT_NORMAL, q.CAIRO.FONT_WEIGHT_BOLD)
        ctx.set_font_size(self.size)
        ctx.set_source_colour(q.Colour.from_rgba((1,0,1,0.6)))
        ctx.move_to((x,y + self.ascent))
        ctx.show_text(self.text)



class Vertical(Container):
    def __init__(self, *children):
        self.children = children


    @property
    def width(self):
        return max(c.width for c in self.children)

    
    @property
    def height(self):
        return sum(c.height for c in self.children)


    def draw(self, ctx, x, y, width, height):
        current_y = y
        for c in self.children:
            c.draw(ctx, x, current_y, width, c.height)
            current_y += c.height



class Horizontal(Container):
    def __init__(self, *children):
        self.children = children


    @property
    def width(self):
        return sum(c.width for c in self.children)

    
    @property
    def height(self):
        return max(c.height for c in self.children)


    def draw(self, ctx, x, y, width, height):
        current_x = x
        for c in self.children:
            c.draw(ctx, current_x, y, c.width, height)
            current_x += c.width


def render_png(layout, path):
    img = q.ImageSurface.create(format=q.CAIRO.FORMAT_RGB24, dimensions=(0,0))
    ctx = q.Context.create(img)

    layout.measure(ctx)

    img = q.ImageSurface.create(format=q.CAIRO.FORMAT_RGB24, dimensions=(int(layout.width), int(layout.height)))
    ctx = q.Context.create(img)
    layout.draw(ctx, 0, 0, layout.width, layout.height)
    img.flush()
    img.write_to_png(path)

